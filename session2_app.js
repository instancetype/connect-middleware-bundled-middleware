/**
 * Created by instancetype on 6/15/14.
 */
var connect = require('connect')

var minute = 60000
  , sessionOpts = { key: 'myapp_sid'
                  , cookie: { maxAge: minute * 1 }
                  }

var app = connect()
  .use(connect.favicon())
  .use(connect.cookieParser('a secret string'))
  .use(connect.session(sessionOpts))
  .use(function(req, res, next) {
         var sess = req.session
         if (sess.views) {
           res.setHeader('Content-Type', 'text/html')
           res.write('<p>views: ' + sess.views + '</p>')
           res.write('<p>expires in: ' + sess.cookie.maxAge/1000 + ' seconds</p>')
           res.write('<p>httpOnly: ' + sess.cookie.httpOnly + '</p>')
           res.write('<p>path: ' + sess.cookie.path + '</p>')
           res.write('<p>domain: ' + sess.cookie.domain + '</p>')
           res.write('<p>secure: ' + sess.cookie.secure + '</p>')
           res.end()
           sess.views++
         } else {
           sess.views = 1
           res.end('Welcome to the session demo. Refresh to increment!')
         }
       }).listen(3000)