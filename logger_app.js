/**
 * Created by instancetype on 6/12/14.
 */
var connect = require('connect'),
    url = require('url')

function hello(req, res) {
    var str = 'Hello World'
    res.setHeader('Content-Type', 'text/plain')
    res.setHeader('Content-Length', str.length)
    res.end('Hello World')
}

// Default logger tokens (headers are not case-sensitive)
//
// :req[header]
// :res[header]
// :http-version
// :response-time
// :remote-addr
// :date
// :method
// :url
// :referrer
// :user-agent
// :status

// Defining a token
connect.logger.token('query-string', function(req, res) {
    return url.parse(req.url).query
})

var app = connect()
    .use(connect.logger(':method :url :query-string :response-time ms'))
    .use(hello)
    .listen(3000)