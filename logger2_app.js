/**
 * Created by instancetype on 6/12/14.
 */
var connect = require('connect')
  ,fs = require('fs')
  ,log = fs.createWriteStream('/var/log/myapp.log', { flags: 'a' })

var app = connect()
  .use(connect.logger({ format: ':method :url', stream: log }))
  .use('/error', error)
  .use(hello)

function hello(req, res) {
  var str = 'Hello World'
  res.writeHeader('Content-Type', 'text/plain')
  res.writeHeader('Content-Length', str.length)
  res.end(str)
}
