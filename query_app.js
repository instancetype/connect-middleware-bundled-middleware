/**
 * Created by instancetype on 6/10/14.
 */
var connect = require('connect');

var app = connect()
    .use(connect.query())
    .use(function(req, res) {
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(req.query));
    }).listen(3000);

// http://localhost:3000/search?images[]=foo.png&images[]=bar.png -> {"images":["foo.png","bar.png"]}