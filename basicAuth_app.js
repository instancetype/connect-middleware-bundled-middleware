/**
 * Created by instancetype on 6/15/14.
 */
var connect = require('connect')
var app1 = connect()
  .use(connect.basicAuth('username-string', 'password-string'))


var users = { dude : 'foo'
            , man  : 'bar'
            , guy  : 'baz'
            }

var app2 = connect()
  .use(connect.basicAuth(function(user, pass) {
    return users[user] === pass
  }))

// Where User.authenticate is a database validation
var User = { authenticate: function() {} }

var app3 = connect()
  .use(connect.basicAuth(function(user, pass, cb) {
    User.authenticate({ user: user, pass: pass }, gotUser)

    function gotUser(err, user) {
      if (err) return cb(err)
      cb(null, user)
    }
  }))

var exampleApp = connect()
  .use(connect.basicAuth('hello', 'world'))
  .use(function(req, res){
         res.end("Credentials accepted!")
       }).listen(3000)