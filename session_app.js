/**
 * Created by instancetype on 6/15/14.
 */
var connect = require('connect')

var app = connect()
  .use(connect.favicon())
  .use(connect.cookieParser('a secret string'))
  .use(connect.session())
  .use(function(req, res, next) {
                   var sess = req.session
                   if (sess.views) {
                     res.setHeader('Content-Type', 'text/html')
                     res.write('<p>views: ' + sess.views + '</p>')
                     res.end()
                     sess.views++
                   } else {
                     sess.views = 1
                     res.end('Welcome to the session demo. Refresh to increment!')
                   }
                 }).listen(3000)
