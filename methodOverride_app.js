/**
 * Created by instancetype on 6/13/14.
 */
var connect = require('connect')

var app = connect()
  .use(connect.logger('dev'))
  .use(connect.bodyParser())
  .use(connect.methodOverride())
  .use(edit)
  .use(update)
  .listen(3000)

function edit(req, res, next) {
  if ('GET' !== req.method) return next()

  res.setHeader('Content-Type', 'text/html')
  res.write('<form method="post">')
  res.write('<input type="hidden" name="_method" value="put">')
  res.write('<input type="text" name="user[name]" value="Jitsu">')
  res.write('<input type="submit" value="Update">')
  res.write('</form>')
  res.end()
}

function update(req, res, next) {
  if ('PUT' !== req.method) return next()
  // methodOverride alters req.method; original is available via req.originalMethod
  res.end('Updated name to ' + req.body.user.name)
}