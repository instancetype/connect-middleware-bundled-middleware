/**
 * Created by instancetype on 6/15/14.
 */
var connect = require('connect')
var app = connect()
  .use(connect.logger('dev'))
  .use(function(req, res, next) {
         setTimeout(function() {
           next(new Error('Something went wrong!'))
         }, 500)
       })
  .use(connect.errorHandler())
  .listen(3000)

// Will respond appropriately to 'Accept: application/json'