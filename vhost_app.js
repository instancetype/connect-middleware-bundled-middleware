/**
 * Created by instancetype on 6/13/14.
 */
var connect = require('connect')
  ,server = connect()
  ,app = require('./sites/instancetype.dev')

server.use(connect.vhost('instancetype', app))
  .listen(3000)

