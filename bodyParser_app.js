/**
 * Created by instancetype on 6/9/14.
 */
var connect = require('connect');
var app = connect()
    .use(connect.bodyParser())
    .use(function(req, res) {
        res.end('Registered new user: ' + req.body.username + '\n');
    }).listen(3000);

// $ curl -d '{"username": "kalymba"}' -H "Content-Type: application/json" http://localhost:3000
// OR
// $ curl -d username=kalymba http://localhost:3000