/**
 * Created by instancetype on 6/15/14.
 */
var connect = require('connect')
var app = connect()
  .use(connect.compress('source'))
  .use(connect.static('source'))

app.listen(3000)

// Using httpie:
// http :3000/script.js Accept-Encoding:gzip -v
// OR
// curl http://localhost:3000/script.js -i -H "Accept-Encoding:gzip"