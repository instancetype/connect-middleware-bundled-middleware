/**
 * Created by instancetype on 6/9/14.
 */
var connect = require('connect');
var app = connect()
    .use(connect.cookieParser('this is a secret string'))
    .use(function(req, res) {
        console.log(req.cookies);
        console.log(req.signedCookies);
        res.end('hello\n');
    }).listen(3000);

// $ curl http://localhost:3000 -H "Cookie: foo=bar; bar=baz"
// OR
// $ curl --cookie "doo=dah; dwee=doot" http://localhost:3000