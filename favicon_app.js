/**
 * Created by instancetype on 6/12/14.
 */
var connect = require('connect')

var app = connect() // favicon() goes first
  .use(connect.favicon()) // takes filepath to .ico as argument or serves default connect favicon
  .use(connect.logger('dev'))
  .use(function (req, res) {
         res.end('Hello world!\n')
       }).listen(3000)

