/**
 * Created by instancetype on 6/15/14.
 */
connect()
  .use(connect.bodyParser())
  .use(connect.cookieParser())
  .use(connect.session())
  .use(connect.csrf()) // MUST GO BELOW bodyParser() and session()
